%% == Erlang Compiler =============================================================================
{erl_opts, [
  debug_info,
  report,
  return,
  %%strict_validation,
  verbose,
  warn_bif_clash,
  warn_export_all,
  warn_export_vars,
  warn_exported_vars,
  %%warn_missing_spec,
  warn_obsolete_guard,
  warn_shadow_vars,
  warn_untyped_record,
  warn_unused_function,
  warn_unused_import,
  warn_unused_record,
  warn_unused_vars,
  warnings_as_errors,
  {parse_transform, lager_transform}
]}.

{dialyzer, [
  %%{base_plt_apps, [crypto, kernel, stdlib]},
  %%{base_plt_location, global}, % global | "/my/file/name"
  {base_plt_prefix, "erl_basics"},
  {get_warnings, true},
  {plt_apps, top_level_deps}, % top_level_deps | all_deps
  %%{plt_extra_apps, []}, % the applications in base_plt_apps will already be in the list
  {plt_location, local}, % local | "/my/file/name"
  {plt_prefix, "erl_basics"},
  %% http://erlang.org/doc/man/dialyzer.html
  {warnings, [
    error_handling,
    no_return,
    %%race_conditions,
    underspecs,
    unknown,
    unmatched_returns
  ]}
]}.

{minimum_otp_vsn, "20"}.

{require_otp_vsn, "20|21"}. %% R16|17|18|19

{validate_app_modules, true}.


%% == Dependencies ================================================================================
{deps, [
  {lager, {git, "git://github.com/erlang-lager/lager.git", {tag, "3.6.2"}}}
]}.

{deps_error_on_conflict, true}.


%% == EUnit & Cover ===============================================================================
{cover_enabled, true}.

%%{cover_export_enabled, true}.

{cover_opts, [verbose]}.

%%{cover_print_enabled, true}.

%%{eunit_compile_opts, [{d, some_define}]}.

{eunit_opts, [
  %%{report, {eunit_surefire, [{dir, "eunit"}]}}, %% TODO
  {verbose}
]}.


%% == EDoc ========================================================================================
{edoc_opts, [
  {packages, false},
  {report_missing_types, true},
  {source_path, ["src"]},
  {subpackages, false},
  {todo, true}
]}.


%% == Profiles ====================================================================================
{profiles, [
  {prod, [
    {erl_opts, [
      no_debug_info,
      warnings_as_errors
    ]},
    {relx, [
      {dev_mode, false},
      {include_erts, true},
      {include_src, false}
    ]}
  ]},
  {test, [
    {deps, [
      %%{hamcrest, {git, "git://github.com/basho/hamcrest-erlang.git", {tag, "0.4.1"}}}
    ]},
    {erl_opts, [
      nowarn_export_all,
      nowarn_missing_spec,
      nowarn_unused_function
    ]}
  ]}
]}.


%% == Release =====================================================================================
{relx, [
  {release, {"erl_basics", "0.1.0"}, [erl_basics]},
  {dev_mode, true},
  {include_erts, false},
  {extended_start_script, true},
  {sys_config, "config/sys.config"},
  {vm_args, "config/vm.args"}
]}.


%% == XRef ========================================================================================
{xref_checks, [
  deprecated_function_calls,
  deprecated_functions,
  exports_not_used,
  locals_not_used,
  undefined_function_calls,
  undefined_functions
]}.

%%{xref_extra_paths, []}.

%%{xref_queries, [{"(XC - UC) || (XU - X - B - \"(rebar.*|mustache)\" : Mod)", []}]}.

{xref_warnings, true}.


%% == Miscellaneous ================================================================================
{alias, [
  {verify, [{clean, "--all"}, eunit, ct]}
]}.


%% == Experimental ================================================================================
%%{clean_files, [".eunit", "ebin/*.beam", "test/*.beam"]}.

%% https://github.com/basho/cuttlefish/blob/develop/rebar.config
%%{escript_emu_args, "%%! -escript main cuttlefish_escript -smp disable +A 0\n"}.
%%{escript_incl_apps, [goldrush, getopt, lager]}.

%%{post_hooks, [
%%  {"-win32", compile, "rebar escriptize"},
%%  {"^((?!-win32).)*$", compile, "./rebar escriptize"}
%%]}.
