# erl_basics
A playground for proof of concepts, tests, etc. using a standard Erlang/OTP application.

## Running Unit Tests
Make sure the code is compiled; then run either `<module>_tests:test()` or `eunit:test(<module>[_tests])`.

Notice that, in the first case, the function `test()` doesn't exist, but it will be automatically "inserted" by EUnit
when it detects the `-include_lib("eunit/include/eunit.hrl").` directive.

## How to Run

```shell
$ rebar3 clean [--all]     # Removes compiled beam files from app(s)
$ rebar3 compile           # Fetches dependencies and compile everything
$ rebar3 dialyzer          # Builds and keeps an up-to-date suitable PLT
$ rebar3 eunit             # Runs eunit test(s) on project app(s)

$ rebar3 shell             # Runs a shell with project apps and deps in path
$ rebar3 auto              # Same as `shell` but with auto compile and load
```

[//]: # ($ rebar3 do clean, eunit)
[//]: # ($ rebar3 verify)

[//]: # ($ erl -pa ebin/)
