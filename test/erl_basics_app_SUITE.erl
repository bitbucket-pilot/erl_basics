-module(erl_basics_app_SUITE).

-compile(export_all).

-define(cfg_lookup(Key, Config), proplists:get_value(Key, Config)).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

%%%================================================================================================
%%% Setup
%%%================================================================================================

all() -> [start_stop].

init_per_suite(Config) ->
  {ok, _} = application:ensure_all_started(erl_basics),
  Config.

end_per_suite(_Config) ->
  application:stop(erl_basics).

%%%================================================================================================
%%% Test Cases
%%%================================================================================================

start_stop(_Config) ->
  X = 15,
  ?assertEqual(true, is_number(X)),
  ok.
