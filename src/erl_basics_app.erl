-module(erl_basics_app).

-behaviour(application).

%% Callback Functions
-export([start/2]).
-export([stop/1]).

%%%================================================================================================
%%% Export
%%%================================================================================================

%%%================================================================================================
%%% Callback Functions
%%%================================================================================================

-spec start(_StartType, _StartArgs) -> ignore | {error, _} | {ok, pid()}.
start(_StartType, _StartArgs) ->
  lager:info("Starting application..."),
  erl_basics_sup:start_link().

-spec stop(_State) -> ignore | {error, _} | {ok, pid()}.
stop(_State) ->
  lager:info("Stopping application..."),
  application:stop(?MODULE).

%%%================================================================================================
%%% Internal
%%%================================================================================================
